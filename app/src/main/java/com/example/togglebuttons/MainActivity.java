package com.example.togglebuttons;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

public class MainActivity extends AppCompatActivity {

    private ToggleButton Botao;
    private TextView Texto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Botao = findViewById(R.id.toggleButtonID);
        Texto = findViewById(R.id.TextID);

        Botao.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Texto.setText("...");
                if(isChecked){
                    Texto.setText("Gokuuuuuuuuuu");
                }
            }
        });
    }
}
